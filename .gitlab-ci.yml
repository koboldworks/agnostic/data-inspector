# You can override the included template(s) by including variable overrides
# See https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#priority-of-environment-variables
image: oven/bun:alpine

stages:
  - test
  - build
  - prepare-release
  - release

cache:
  paths:
    - node_modules

variables:
  GIT_STRATEGY: fetch
  GIT_DEPTH: "1"
  PACKAGE_NAME: data-inspector
  PACKAGE_TYPE: module
  PACKAGE_REGISTRY_URL: $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/$PACKAGE_NAME
  BUILD_DIRECTORY: dist

compile:
  stage: build
  before_script:
    - bun install --frozen-lockfile
  script:
    - bun run build
    - mv $BUILD_DIRECTORY $PACKAGE_NAME
  artifacts:
    paths:
      - $PACKAGE_NAME
  rules:
    - if: '$CI_COMMIT_TAG !~ "latest"'
    - if: '$CI_COMMIT_TAG =~ /^\d+\.\d+\.\d+\.\d+$/'

publish_artifacts:
  stage: prepare-release
  image: alpine:latest
  needs:
    - compile
  before_script:
    - apk update
    - apk add zip curl
  script:
    - cd $PACKAGE_NAME
    - zip -r ../$PACKAGE_NAME.zip .
    - cd ..
    - |
      echo "Publishing $PACKAGE_NAME $CI_COMMIT_TAG to $PACKAGE_REGISTRY_URL"
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $PACKAGE_NAME.zip "$PACKAGE_REGISTRY_URL/$CI_COMMIT_TAG/$PACKAGE_NAME.zip"
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $PACKAGE_NAME/$PACKAGE_TYPE.json "$PACKAGE_REGISTRY_URL/$CI_COMMIT_TAG/$PACKAGE_TYPE.json"
  rules:
    - if: '$CI_COMMIT_TAG !~ "latest"'
    - if: '$CI_COMMIT_TAG =~ /^\d+\.\d+\.\d+\.\d+$/'

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - publish_artifacts
  rules:
    - if: '$CI_COMMIT_TAG !~ "latest"'
    - if: '$CI_COMMIT_TAG =~ /^\d+\.\d+\.\d+\.\d+$/'
  script:
    - |
      release-cli create --name "${CI_COMMIT_TAG}" \
      --tag-name $CI_COMMIT_TAG \
      --assets-link "{\"name\":\"$PACKAGE_NAME.zip\",\"url\":\"$PACKAGE_REGISTRY_URL/$CI_COMMIT_TAG/$PACKAGE_NAME.zip\",\"filepath\":\"/$PACKAGE_NAME.zip\",\"link_type\":\"package\"}" \
      --assets-link "{\"name\":\"$PACKAGE_TYPE.json\",\"url\":\"$PACKAGE_REGISTRY_URL/$CI_COMMIT_TAG/$PACKAGE_TYPE.json\",\"filepath\":\"/$PACKAGE_TYPE.json\",\"link_type\":\"other\"}"
