export const CFG = {
	id: 'data-inspector',
	SETTINGS: {
		allowEditing: 'allowEditing',
		allowEditingUnsupported: 'allowEditingUnsupported',
		allowUsers: 'allowUsers',
		functions: 'functions',
		results: 'results',
		mode: 'mode',
		tooltip: 'tooltip',
	},
	COLORS: {
		main: 'color:deepskyblue',
		number: 'color:mediumpurple',
		label: 'color:mediumseagreen',
		id: 'color:darkseagreen',
		unset: 'color:unset',
	},
	typeIcons: {
		array: 'fa-solid fa-list-ol', //
		number: 'fa-solid fa-hashtag', // 🔢
		string: '', // 🔤
		object: 'fa-solid fa-box', // 📦
		model: 'fa-solid fa-pizza-slice', // 🍕

	},
};
