export class SearchResult {
	/** @type {string} */
	path;

	/** @type {boolean} */
	keyFocus = true;

	/** @type {boolean} */
	match;

	/** @type {boolean} */
	get mismatch() { return this.indexes.path.length === 0; }

	/**
	 * Search target, what is being looked for.
	 *
	 * @type {object}
	 */
	target;
	/** @type {number} */
	score;

	/** @type {boolean} */
	keyFullMatch;
	/** @type {boolean} */
	pathFullMatch;

	indexes = {
		/** @type {number[]} */
		path: [],
		/** @type {number[]} */
		key: [],
	};

	/** @type {number} */
	ratio;
	/** @type {boolean} */
	get good() { return this.ratio > 0.8; }
	/** @type {boolean} */
	get bad() { return this.ratio < 0.3; }
	scoring = {
		/** @type {number} */
		full: undefined,
		/** @type {number} */
		key: undefined,
	};

	constructor({ path, match, target, score, keyFocus, keyFullMatch, pathFullMatch, pathIndexes, keyIndexes, ratio, scoring } = {}) {
		this.path = path;
		this.match = match;
		this.target = target;

		this.keyFocus = keyFocus;

		this.score = score;
		this.keyFullMatch = keyFullMatch;
		this.pathFullMatch = pathFullMatch;
		this.indexes.path = pathIndexes;
		this.indexes.key = keyIndexes;

		this.ratio = ratio;

		this.scoring = scoring;
	}
}
