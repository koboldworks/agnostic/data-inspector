export class Score {
	/** @type {number} */
	value;
	/** @type {string} */
	description;

	/**
	 * @param {number} value
	 * @param {string} description
	 */
	constructor(value, description) {
		this.value = value;
		this.description = description;
	}
}
