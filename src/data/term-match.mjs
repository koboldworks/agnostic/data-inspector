export class TermMatch {
	/** @type {number} */
	score;
	/** @type {number[]} */
	indexes;
	full;
	half;
	/** @type {number} */
	ratio;
	/** @type {number} */
	consecutives;
	/** @type {number} */
	longestChain;
	/** @type {Score[]} */
	scoring;

	constructor({ indexes, full, half, ratio, consecutives, longestChain, scoring } = {}) {
		this.indexes = indexes;
		this.full = full;
		this.half = half;
		this.ratio = ratio;
		this.consecutives = consecutives;
		this.longestChain = longestChain;
		this.scoring = scoring;
		this.score = scoring.reduce((old, scr) => old + scr.value);
	}
}
