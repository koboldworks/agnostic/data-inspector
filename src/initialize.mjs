import { CFG } from './config.mjs';
import { DataInspectorDialog } from './inspection-dialog.mjs';

/**
 * @param {Actor|Item} document
 * @param doc
 */
function openInspector(doc) {
	const oldApp = Object.values(doc.apps)
		.find(app => app instanceof DataInspectorDialog);
	if (oldApp) oldApp.render(true, { focus: true });
	else new DataInspectorDialog(doc).render(true);
};

function injectHeaderDataButton(sheet, buttons) {
	const allowUsers = game.settings.get(CFG.id, CFG.SETTINGS.allowUsers);
	if (!allowUsers && !game.user.isGM) return;

	// Exhaustive document lookup
	const document = sheet.document ?? sheet.actor ?? sheet.item ?? sheet.object;
	if (!(document instanceof foundry.abstract.Document)) throw new Error('Could not locate sheet\'s document');

	buttons.unshift({
		class: 'data-inspector',
		icon: 'fas fa-atom',
		label: game.i18n.localize('DataInspector.DataButton'),
		onclick: _ => openInspector(document),
	});
};

/**
 * @param {*} app HTML in v11 and prior, app in v12 and onward
 * @param {object[]} entries
 */
function injectContextDataButton(app, entries) {
	let directoryId, packId;
	if (game.release.generation >= 12) {
		directoryId = app.id;
		packId = app.metadata?.id;
	}
	else {
		const html = app[0];
		directoryId = html.dataset.tab;
		packId = html.dataset.pack;
	}

	if (packId) {
		const p = game.packs.get(packId);
		if (!p) return;
		if (!['Item', 'Actor'].includes(p.metadata.type)) return;
	}

	entries.push({
		name: 'DataInspector.Context.OpenInspector',
		icon: '<i class="fas fa-atom"></i>',
		condition: () => {
			const allowUsers = game.settings.get(CFG.id, CFG.SETTINGS.allowUsers);
			return allowUsers || game.user.isGM;
		},
		callback: async ([entry]) => {
			const documentId = entry.dataset.documentId;

			let doc;
			if (packId)
				doc = await game.packs.get(packId)?.getDocument(documentId);
			else if (directoryId === 'items')
				doc = game.items.get(documentId);
			else if (directoryId === 'actors')
				doc = game.actors.get(documentId);
			else
				return void console.error('Unknown data source:', { documentId, directoryId, packId, entry });

			if (doc)
				openInspector(doc);
			else
				console.warning('Document not found:', { documentId, directoryId, packId });
		},
	});
};

Hooks.once('ready', async () => {
	const C = CFG.COLORS;

	const allowUsers = game.settings.get(CFG.id, CFG.SETTINGS.allowUsers);
	if (!allowUsers && !game.user.isGM)
		return void console.log(`%cDATA INSPECTOR%c 🔍 | %c${mod.version}%c | User access blocked in settings.`, C.main, C.unset, C.id, C.unset);

	// Load templates
	const templates = ['object', 'inspector', 'value', 'object-data'];
	await loadTemplates(templates.map(t => `modules/${CFG.id}/template/${t}.hbs`));

	// Register API
	const mod = game.modules.get(CFG.id);
	mod.api = {
		inspect: openInspector,
	};

	// Fetch in-built system specific extensions
	// const systemExtPath = `modules/${CFG.module}/systems/${game.system.id}.mjs`;

	if (['pf1'].includes(game.system.id)) { // Temporary limiter on import() errors
		await import(`systems/${game.system.id}.mjs`)
			.then(() => console.log('%cDATA INSPECTOR%c 🔍 | System extensions loaded %c✔', C.main, C.unset, 'color:green'))
			.catch(() => console.log('%cDATA INSPECTOR%c 🔍 | System extensions not found %c✘', C.main, C.unset, 'color:red'));
	}

	console.log(`%cDATA INSPECTOR%c 🔍 | %c${mod.version}%c | READY`, C.main, C.unset, C.id, C.unset);
});

/**
 * HACK for Foundry v12
 *
 * @param {ApplicationV2} app
 * @param {HTMLElement} el
 */
function injectHeaderDataButtonV2(app, el) {
	const doc = app.document;
	if (!(doc instanceof Actor || doc instanceof Item)) return;
	if (!doc?.testUserPermission?.(game.user, 'OBSERVER')) return;

	const header = el.querySelector('header.window-header');
	if (!header) return;
	if (header.querySelector('button[data-action="__mdiInspectData"]')) return; // Already there
	const rel = header.querySelector('button[data-action="copyUuid"]');
	if (!rel) return; // Didn't find copy UUID button

	const b = document.createElement('button');
	b.type = 'button';
	b.dataset.action = '__mdiInspectData';
	b.dataset.tooltip = game.i18n.localize('DataInspector.Context.OpenInspector');
	b.classList.add('header-control', 'fa-solid', 'fa-atom', 'data-inspector');
	rel.before(b);

	app.options.actions.__mdiInspectData ??= function (_event, _el) {
		openInspector(this.document);
	};
}

Hooks.on('renderDocumentSheetV2', injectHeaderDataButtonV2);

// Header buttons
Hooks.on('getActorSheetHeaderButtons', injectHeaderDataButton);
Hooks.on('getItemSheetHeaderButtons', injectHeaderDataButton);

// Context menus
Hooks.on('getItemDirectoryEntryContext', injectContextDataButton);
Hooks.on('getActorDirectoryEntryContext', injectContextDataButton);
Hooks.on('getCompendiumEntryContext', injectContextDataButton);
