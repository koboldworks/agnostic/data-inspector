import { CFG } from './config.mjs';
import { getTypeName, getTypeId, DATA_TYPES, DATA_TYPE_INVERTED } from './data-types.mjs';

const fu = foundry.utils;

/**
 *
 * @param {object} update Update prop
 * @param {object} parent Parent prop data
 * @param {object} options
 * @param {boolean} options.isDelete Is this a delete operation?
 * @param options.inArray
 * @param options.isUpdate
 */
const setUpdateData = (update, parent, { inArray = false, isUpdate = false, isDelete = false } = {}) => {
	const opLabel = isDelete ? 'Delete' : 'Update';

	const { value, last: updateProp } = update;

	// Update or delete array index
	const C = CFG.COLORS;
	if (Array.isArray(updateProp)) {
		const index = Number(parent.child);
		console.log(`${opLabel} index %c${index}%c in %c${parent.path}%c`,
			C.number, C.unset, C.id, C.unset);

		if (isDelete)
			updateProp.splice(index, 1);
		else {
			console.log('=', value);
			updateProp.splice(index, 1, value);
		}
	}
	// Update or delete other prop
	else {
		console.log(`${opLabel} value %c${parent.child}%c in %c${parent.path}%c`,
			C.id, C.unset, C.id, C.unset);

		if (isDelete) {
			delete updateProp[parent.child];
			// Normal delete only necessary if we aren't inside an array
			if (!inArray) updateProp['-=' + parent.child] = null;
		}
		else {
			console.log('... Value =', value);
			updateProp[parent.child] = value;
		}
	}
};

const getParentProperty = (path, docData) => {
	const parts = path.split('.');
	const lastProp = parts.pop();
	const parentPath = parts.join('.');
	const parent = fu.getProperty(docData, parentPath);
	return { path: parentPath, value: parent, child: lastProp };
};

// Restructure array parents
const reconstructParentArrays = (docData, path) => {
	const parts = path.split('.');

	const updateData = {};
	let currentUpdateProp = updateData,
		current = docData;

	let inArray = false;

	do {
		const prop = parts.shift();
		const value = current[prop];
		if (Array.isArray(value)) {
			currentUpdateProp[prop] = foundry.utils.deepClone(value);
			inArray = true;
		}
		else
			currentUpdateProp[prop] ??= {};
		currentUpdateProp = currentUpdateProp[prop];
		current = current[prop];
	} while (parts.length);

	return { updateData, last: currentUpdateProp, inArray };
};

export class ValueEditor extends FormApplication {
	_path;
	_type;
	/** @type {Actor|Item} */
	document;

	/** @type {DataInspectorDialog} */
	parentApp;

	static get supportedTypes() { return [DATA_TYPES.number, DATA_TYPES.string, DATA_TYPES.boolean, DATA_TYPES.null, DATA_TYPES.undefined]; }

	constructor(document, path, type, { isToken = false, app, inArray = false } = {}) {
		super(document);

		this.document = document;

		this.parentApp = app;
		this.isToken = isToken;
		this._path = path;
		// this.object = { value: undefined };
		this.document = document;
		const value = fu.getProperty(document.toObject(), path);
		this.originalTypeId = type ?? getTypeId(value);
		this.originalTypeName = DATA_TYPE_INVERTED[this.originalTypeId];
		this._type = this.originalTypeName;

		this.inArray = inArray;

		// Register for updates
		document.apps[this.appId] = this;
	}

	get title() {
		const title = ['Data Inspector'];
		title.push(this.document.name);
		title.push(this._path);
		return title.join(': ');
	}

	get id() {
		return 'data-inspector-' + this.document.uuid.replaceAll('.', '-') + '-' + this._path.replaceAll('=', '-');
	}

	get template() {
		return `modules/${CFG.id}/template/editor.hbs`;
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: [...options.classes, 'koboldworks', 'data-inspector-editor'],
			submitOnChange: false,
			submitOnClose: false,
			closeOnSubmit: false,
			resizable: true,
			width: 520,
			height: 360,
		};
	}

	async getData() {
		// console.log('Editor.getData');
		const data = super.getData();

		const doc = this.document,
			value = fu.getProperty(doc.toObject(), this._path),
			typeId = this._typeId = getTypeId(value),
			typeName = this._type ?? getTypeName(value),
			originalTypeName = this.originalTypeName;

		// Required by {{editor}} to function
		this.object = { value };

		data.document = doc;
		data.path = this._path;
		data.value = value;
		data.type = typeName;
		data.typeName = typeName;
		data.originalType = originalTypeName;
		data.similarType = originalTypeName === 'string' && ['text', 'html'].includes(typeName);
		data.isHTML = typeName === 'html';
		data.isString = ['html', 'text', 'string'].includes(typeName);
		data.isLongString = typeName === 'html' || typeName === 'text' || originalTypeName === 'string' && (data.value.length > 16 || data.value.indexOf('\n') >= 0);
		data.isUnsupported = !ValueEditor.supportedTypes.includes(typeId);
		data.noUpdate = typeName === 'undefined';

		data.isActor = doc instanceof Actor;
		data.isItem = doc instanceof Item;
		data.isEditable = !data.isUnsupported;
		data.isOwner = doc.isOwner;
		data.simpleType = ['number', 'string', 'boolean'].includes(typeName);
		data.deadType = ['null', 'undefined'].includes(typeName);
		data.selectedType = this._type;
		data.types = {
			number: 'Number',
			boolean: 'Boolean',
			string: 'String',
			null: 'Null',
			undefined: 'Undefined',
			text: 'Text Block',
			html: 'HTML',
		};

		data.enriched = data.isHTML ? await TextEditor.enrichHTML(this.object.value, { async: true }) : null;

		data.isToken = this.isToken;

		data.inArray = this.inArray;

		data.TYPES = DATA_TYPES;
		return data;
	}

	/**
	 * @param {Event} event
	 */
	_deleteData(event) {
		event.preventDefault();
		event.stopPropagation();

		const doc = this.document,
			path = this._path;

		const docData = doc.toObject();

		const parent = getParentProperty(path, docData);
		const update = reconstructParentArrays(docData, parent.path);
		const { updateData, inArray } = update;
		setUpdateData(update, parent, { isDelete: true, inArray });

		console.log('🔎 ⌦ UpdateData:', updateData);

		const app = this;
		Dialog.confirm({
			title: 'Delete data?',
			content: `<p>Delete: <code>${path}</code></p>`,
			yes: () => {
				app.close();
				doc.update(updateData);
			},
			defaultYes: false,
		});
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];
		const app = this;

		html.querySelector('select[name="type"]')?.addEventListener('change', (ev) => {
			ev.preventDefault();
			ev.stopPropagation();
			const oldType = this._type,
				newType = ev.target.value;
			this._type = newType;
			// console.log('Changing type from', oldType, 'to', newType);
			this.render();
		});

		// Type swapping
		html.querySelector('select[name="type"]')
			?.addEventListener('change', function (ev) {
				ev.preventDefault();
				ev.stopPropagation();
				app._type = this.value;
				app.editors = {}; // Reset {{editor}}
				app.render(true);
			});

		// Delete button listener
		html.querySelector('button.delete')
			?.addEventListener('click', app._deleteData.bind(app));
	}

	_updateObject(event, formData) {
		event.preventDefault();
		event.stopPropagation();

		// delete formData.type;
		const type = this._type,
			path = this._path,
			doc = this.document;

		let value = formData.value;
		if (type === 'undefined') value = undefined;
		else if (type === 'null') value = null;

		console.log('Setting data:', { type, path, value });

		const docData = doc.toObject();

		const parent = getParentProperty(path, docData);
		const update = reconstructParentArrays(docData, parent.path);
		const { updateData } = update;

		update.value = value;
		setUpdateData(update, parent, { isUpdate: true });

		console.log('🔎 ＋ UpdateData:', updateData);

		// Prevent this app being refreshed by the update
		delete this.document.apps[this.appId];

		try {
			doc.update(updateData);
		}
		finally {
			this.close();
		}
	}

	close(...args) {
		delete this.document.apps[this.appId];
		super.close(...args);
	}
}
