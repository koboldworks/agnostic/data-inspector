import { getTypeId, getChildTypeId, getModelType, DATA_TYPES, DATA_TYPE_INVERTED } from './data-types.mjs';

const fu = foundry.utils;

const dataKeySorter = (a, b) => a.key.localeCompare(b.key);

/**
 * Stub data model for figuring out elements to ignore in them.
 */
class StubModel extends (foundry.abstract.TypeDataModel || foundry.abstract.DataModel) {
	static defineSchema() { return {}; }
}

const ignoreDataModelParts = Object.getOwnPropertyNames(new StubModel());

class ChildKeyData {
	key;
	type;
	constructor(key, type) {
		this.key = key;
		this.type = type;
	}
}

export class DataStructure {
	/** @type {string} */
	key;
	/** @type {string} */
	path;
	get basePath() {
		return this.path.replace(/^system\./, '');
	}

	/** @type {string} */
	get type() { return DATA_TYPE_INVERTED[this.typeId]; }

	/** @type {number} */
	typeId;

	/** @type {DataStructure} */
	root;

	/** @type {*} */
	value;

	/** @type {DataStructure} */
	parent;
	/** @type {DataStructure[]} */
	children = [];
	/** @type {number} */
	depth = 0;
	/** @type {string} */
	treetype;

	get isRollData() {
		return this.root.treetype === 'rolldata';
	}

	/** @type {object | undefined} */
	_sourceData;
	/** @type {object | undefined} */
	_derivedData;
	/** @type {object | undefined} */
	_rollData;
	/** @type {object|undefined} */
	_overides;

	/** @type {boolean} */
	includeFunctions = false;

	/** @type {boolean} */
	get isBigString() { return this.typeId === DATA_TYPES.string && this.value?.length > 24; }

	/** @type {boolean} */
	get isNull() { return this.typeId === DATA_TYPES.null; }

	/** @type {boolean} */
	get isFunction() { return this.typeId === DATA_TYPES.function; }

	/** @type {boolean} */
	get isGetter() { return this.typeId === DATA_TYPES.getter; }

	/** @type {boolean} */
	get isUndefined() { return this.typeId === DATA_TYPES.undefined; }

	/** @type {boolean} */
	get isNullish() {
		if ([DATA_TYPES.null, DATA_TYPES.undefined].includes(this.typeId)) return true;
		if (this.isArray && this.value.length === 0) return true;
		if (this.isSet && this.value.size === 0) return true;
		if ((this.isMap || this.isCollection) && this.value.size == 0) return true;
		return false;
	}

	/** @type {string|null} */
	get identifier() {
		switch (this.typeId) {
			case DATA_TYPES.custom:
			case DATA_TYPES.model: return this.value.constructor.name;
			default: return null;
		}
	}

	get isModel() { return this.typeId === DATA_TYPES.model; }

	/** @type {boolean} */
	get isArray() { return this.typeId === DATA_TYPES.array; }

	get isSet() { return this.typeId === DATA_TYPES.set; }

	get isArrayLike() { return this.isArray || this.isSet; }

	get isMap() { return this.typeId === DATA_TYPES.map; }

	get isCollection() { return this.typeId === DATA_TYPES.collection; }

	/** @type {boolean} */
	get isObject() { return this.typeId === DATA_TYPES.object; }

	get isUndesirable() {
		return this.isDocument || [DATA_TYPES.placeable, DATA_TYPES.pixi, DATA_TYPES.app].includes(this.typeId);
	}

	/** @type {boolean} */
	get isDocument() { return [DATA_TYPES.document].includes(this.typeId); }

	/** @type {string} */
	get documentId() {
		if (this.typeId === DATA_TYPES.document) return this.value.id;
		return undefined;
	}

	get hasValues() {
		if (this.isContainer && !this.isArrayLike) {
			return ('total' in this.value || 'value' in this.value || 'max' in this.value);
		}
		return false;
	}

	get values() {
		const data = {
			value: this.value.value,
			haveValue: false,
			max: this.value.max,
			haveMax: false,
			isValue: false,
			total: this.value.total,
			haveTotal: false,
			noValues: false,
		};

		if (data.value === null || (typeof data.value === 'number')) {
			data.value = `${data.value}`;
			data.haveValue = true;
		}
		if (data.max === null || (typeof data.max === 'number')) {
			data.max = `${data.max}`;
			data.haveMax = true;
		}
		if (data.total === null || (typeof data.total === 'number')) {
			data.total = `${data.total}`;
			data.haveTotal = true;
		}
		if (data.haveValue || data.haveMax) {
			data.isValue = true;
			data.isValueOf = data.haveValue && data.haveMax;
		}

		if (!data.isValue && !data.haveTotal) data.noValues = true;

		return data;
	}

	/** @type {string} */
	get className() { return this.value.__proto__?.constructor.name; }

	/** @type {boolean} */
	get isEmpty() {
		if (this.isNullish) return true;
		switch (this.typeId) {
			case DATA_TYPES.array:
				return this.value.length == 0;
			case DATA_TYPES.object:
				return fu.isEmpty(this.value);
			case DATA_TYPES.map:
			case DATA_TYPES.collection:
				return this.value.size === 0;
			case DATA_TYPES.custom:
				return this.children.length == 0;
		}
		return false;
	}

	recursionPoint = false;

	/** @type {boolean} */
	get isPrimitive() {
		return this.isBoolean || this.isNumber || this.isString || this.isNull || this.isUndefined;
	}

	/** @type {boolean} */
	get isBoolean() { return this.typeId === DATA_TYPES.boolean; }
	/** @type {boolean} */
	get isString() { return this.typeId === DATA_TYPES.string; }
	/** @type {boolean} */
	get isNumber() { return this.typeId === DATA_TYPES.number; }

	/** @type {boolean} */
	get inArray() { return this.parent?.isArrayLike ?? false; }

	/** @type {boolean} */
	get isContainer() {
		if (this.recursionPoint) return false;
		return [DATA_TYPES.array, DATA_TYPES.set, DATA_TYPES.object, DATA_TYPES.model, DATA_TYPES.map, DATA_TYPES.collection, DATA_TYPES.custom].includes(this.typeId);
	}

	get isChildless() { return [DATA_TYPES.boolean, DATA_TYPES.string, DATA_TYPES.null, DATA_TYPES.undefined, DATA_TYPES.number].includes(this.typeId); }

	/** @type {boolean} */
	get isCustom() { return this.typeId === DATA_TYPES.custom; }

	/** @type {string} */
	get formattedValue() {
		switch (this.typeId) {
			case DATA_TYPES.undefined: return 'undefined';
			case DATA_TYPES.null: return 'null';
			case DATA_TYPES.function: return 'function';
		}
		return this.value;
	}

	#inRoll;
	/** @type {boolean} */
	get inRollData() {
		if (this.#inRoll === undefined) {
			if (this.root.treetype === 'rolldata') return this.#inRoll = true;
			if (!this.root._rollData) return false;
			const path = this.basePath;
			try {
				// BUG: This fails with Set
				return this.#inRoll = fu.hasProperty(this.root._rollData, path);
			}
			catch (err) {
				/* fu.hasProperty() doesn't like "invalid" data  */
			}
			return this.#inRoll = 3;
		}
		return this.#inRoll;
	}

	#inSource;
	/** @type {boolean} */
	get inSourceData() {
		if (this.#inSource === undefined) {
			if (this.root.treetype === 'source')
				return this.#inSource = true;
			if (!this.root._sourceData)
				return false;
			const path = this.basePath;
			try {
				const data = this.root._sourceData;
				return (this.#inSource = fu.hasProperty(data, path));
			}
			catch (err) {
				/* fu.hasProperty() doesn't like "invalid" data  */
			}
			return (this.#inSource = 3);
		}
		return this.#inSource;
	}

	#inDerived;
	/** @type {boolean} */
	get inDerivedData() {
		if (this.#inDerived === undefined) {
			if (this.root.treetype === 'derived') return this.#inDerived = true;
			if (!this.root._derivedData) return false;
			const path = this.basePath;
			try {
				const data = this.root._derivedData;
				return this.#inDerived = fu.hasProperty(data, path);
			}
			catch (err) {
				/* fu.hasProperty() doesn't like "invalid" data  */
			}
			return this.#inDerived = 3;
		}
		return this.#inDerived;
	}

	#inTemporary;
	/** @type {boolean} */
	get inTemporaryData() {
		if (this.#inTemporary === undefined) {
			if (!this.root._temporaryData) return false;
			const path = this.basePath;
			try {
				const data = this.root._temporaryData;
				return this.#inTemporary = fu.hasProperty(data, path);
			}
			catch (err) {
				/* fu.hasProperty() doesn't like "invalid" data  */
			}
			return this.#inTemporary = 3;
		}
		return this.#inTemporary;
	}

	get hasOverrides() {
		return !!this._overrides;
	}

	/** @type {string} */
	get css() {
		if (this.typeId === DATA_TYPES.boolean)
			return this.value ? 'true' : 'false';
		if (this.typeId === DATA_TYPES.number)
			return this.value === 0 ? 'zero' : this.value < 0 ? 'negative' : 'positive';
		return '';
	}

	/**
	 * @param {object} options
	 * @param {string} options.key
	 * @param {string} options.path
	 * @param {*} options.value
	 * @param {DataStructure} options.parent
	 * @param {DATA_TYPES} options.type
	 * @param {"rolldata"|"derived"|"source"} options.treeType
	 * @param {number} options.depth
	 */
	constructor({ key, path, value, parent, type, treeType, depth }) {
		this.key = key;
		this.path = path;
		this.typeId = type ?? getTypeId(value);
		this.value = value;
		this.parent = parent;
		this.root = parent?.root ?? parent;
		this.depth = depth;
		this.treetype = treeType;

		this.includeFunctions = parent?.includeFunctions ?? false;

		// if (this.typeId === DATA_TYPES.custom) console.log(this.type, this.typeId, value, value.constructor.name);
	}

	#childKeys;
	/** @type {ChildKeyData[]} */
	childKeys() {
		if (this.#childKeys) return this.#childKeys;
		if (this.isUndesirable) return [];
		switch (this.typeId) {
			case DATA_TYPES.set:
				return this.#childKeys = [Array.from(this.value).map(k => new ChildKeyData(k))];
			case DATA_TYPES.array:
				return this.#childKeys = [this.value.map(k => new ChildKeyData(k))];
			case DATA_TYPES.collection:
			case DATA_TYPES.map:
				return this.#childKeys = [Array.from(this.value.keys()).map(k => new ChildKeyData(k))];
			case DATA_TYPES.model:
			case DATA_TYPES.object:
			case DATA_TYPES.custom: {
				const allKeys = [], rvg = [], rvf = [];
				const props = Object.getOwnPropertyNames(this.value).filter(k => {
					if (this.isModel)
						return !ignoreDataModelParts.includes(k);
					return true;
				});
				allKeys.push(...props.map(k => new ChildKeyData(k)));

				if (this.includeFunctions) {
					// Find getters also
					let proto = this.value;
					if ([DATA_TYPES.custom, DATA_TYPES.object, DATA_TYPES.model].includes(this.typeId)) {
						const ignored = ['constructor', 'toString', 'almostEqual', '__proto__', 'prototype', 'between', 'toObject', 'toJSON', 'clone'];
						const ignoreModelBits = [...ignoreDataModelParts, 'schema', 'validationFailures', 'invalid', 'validate', 'update', 'updateSource', 'reset'];
						do {
							const keys = Object.getOwnPropertyNames(proto);
							for (const key of keys) {
								if (key.startsWith('_')) continue;
								if (ignored.includes(key)) continue;

								if (this.isModel && ignoreModelBits.includes(key)) continue;

								const cType = getChildTypeId(proto, key);
								if (cType === DATA_TYPES.getter)
									rvg.push(new ChildKeyData(key, DATA_TYPES.getter));
								else if (cType === DATA_TYPES.function)
									rvf.push(new ChildKeyData(key, DATA_TYPES.function));
							}

							proto = Object.getPrototypeOf(proto);
							if (proto?.__proto__ == null) break;
						} while (proto);
					}
				}
				return this.#childKeys = [allKeys.sort(dataKeySorter), rvg.sort(dataKeySorter), rvf.sort(dataKeySorter)];
			}
			default:
				return this.#childKeys = [];
		}
	}

	fillChildren() {
		if (this.recursionPoint) return [];

		const curpath = this.path,
			basePath = curpath?.length ? curpath : this.key,
			isArrayLike = this.isArrayLike,
			isMap = this.isMap || this.isCollection,
			isSet = this.isSet;

		const saferValue = isSet ? Array.from(this.value) : this.value;

		const parseChildren = (list) => {
			list?.forEach((ckData, i) => {
				const { key, type } = ckData;
				const childKey = !isArrayLike ? key : i;
				const path = basePath ? `${basePath}.${childKey}` : childKey;

				const hookId = 'data-inspector.filterData';
				if (Hooks.events[hookId]) {
					if (Hooks.call(hookId, this.root.document, path, this.treetype) === false) return;
				}

				const value = isMap ? this.value.get(childKey) : saferValue[childKey],
					typeId = getTypeId(value);

				if (typeId === DATA_TYPES.function && !this.includeFunctions) return; // Not including functions

				const c = new DataStructure({ key: childKey, path, value, parent: this, type, depth: this.depth + 1 });
				this.children.push(c);
			});
		};

		const [all, functions, getters] = this.childKeys();

		parseChildren(all);
		parseChildren(functions);
		parseChildren(getters);

		return this.children;
	}

	static recurse(sourceData, key, path, type, { includeFunctions = false, document } = {}) {
		this.includeFunctions = includeFunctions ?? false;

		const dt = new DataStructure({ key, path, value: sourceData, parent: null, treeType: type, depth: 0 });
		dt.document = document;
		dt.root = dt;
		const all = {};
		if (dt.path) all[dt.path] = dt;
		let itemCount = 0, error = false, maxDepth = 0;

		const visitedNodes = new Map();

		/**
		 * @param {DataStructure} ds
		 */
		const handleChild = (ds) => {
			all[ds.path] = ds;

			if (maxDepth < ds.depth) maxDepth = ds.depth;

			itemCount++;
			if (itemCount > 25000) {
				if (!error) {
					ui.notifications.error('DATA INSPECTOR | Possible cyclic reference encountered; ending inspection early', { console: false });
					console.error(ds.path, '\nDATA INSPECTOR | Possible cyclic reference encountered; ending inspection early\n', ds);
					error = true;
				}
				return; // Arbitrary hard exit
			}

			if (!ds.isPrimitive) {
				const old = visitedNodes.get(ds.value);
				if (old) {
					ds.recursionPoint = old;
					return;
				}
				else
					visitedNodes.set(ds.value, ds.path);
			}

			try {
				recurseChildren(ds);
			}
			catch (err) {
				console.error(err, this);
				throw err;
			}
		};

		/**
		 * @param {DataStructure} ds
		 */
		const recurseChildren = (ds) => {
			if (!ds.isChildless) {
				ds.fillChildren()
					.forEach(handleChild);
			}
		};

		recurseChildren(dt);

		return { root: dt, all, count: itemCount, depth: maxDepth };
	}

	/*
	hasChild(path) {
		const parts = path.split('.');
	}
	*/

	/**
	 * @param {string} path
	 * @returns {DataStructure}
	 */
	getAtPath(path) {
		const parts = path.split('.');
		const key = parts.shift();

		const getPart = () => {
			switch (this.typeId) {
				case DATA_TYPES.set:
				case DATA_TYPES.array: return this.children[parseInt(key)];
				default: return this.children.find(cd => cd.key === key);
			}
		};

		const c = getPart();

		if (parts.length === 0) return c;
		else if (c) return c.getAtPath(parts.join('.'));
	}
}
