import { CFG } from './config.mjs';
import { getTypeId, getChildTypeId, DATA_TYPES, DATA_TYPE_INVERTED } from './data-types.mjs';
import { ValueEditor } from './data-editor.mjs';
import { DataStructure } from './data-structure.mjs';

import { SearchResult } from './data/search-result.mjs';
import { TermMatch } from './data/term-match.mjs';
import { Score } from './data/score.mjs';

const fu = foundry.utils;

/**
 * @param {Actor|Item} doc Original document to mimic
 * @returns {Actor|Item} New temporary document of same type with minimal initialization.
 */
function createTemporaryDocument(doc) {
	const data = { name: doc.name + ' [Temporary]', type: doc.type, system: {} };
	const hookId = 'data-inspector.temporaryData';
	if (Hooks.events[hookId]) Hooks.callAll(hookId, doc, data);
	return doc instanceof Actor ? new Actor.implementation(data) : new Item.implementation(data);
}

export class DataInspectorDialog extends DocumentSheet {
	static EDITABLE_MODES = ['source', 'override', 'flags'];

	_path = '';
	/** @type {Element} */
	_pathEl;
	_search = '';
	_searchEnd = true;
	_functions = false;
	_document = false;
	_mode = 'rolldata';
	_expandedItems = new Set();
	_derivedData;
	_sourceData;
	_temporaryData;
	_rollData;
	_flagData;
	_overrides;
	_resultQuality = 0.7;
	/** @type {DataStructure} */
	root;

	paths;

	/**
	 * @param {Actor|Item} document
	 * @param {object} options
	 */
	constructor(document, options = {}) {
		super(document, options);

		if (!document.testUserPermission(game.user, 'OBSERVER')) {
			const msg = 'Insufficient permissions to inspect document';
			ui.notifications.error(msg, { console: false });
			throw new Error(msg);
		}

		this._functions = game.settings.get(CFG.id, CFG.SETTINGS.functions);
		this._resultQuality = game.settings.get(CFG.id, CFG.SETTINGS.results);
		this._mode = game.settings.get(CFG.id, CFG.SETTINGS.mode);
	}

	get isEditable() { return true; }

	get title() {
		const doc = this.document;
		const parent = doc.actor;

		const title = ['Data Inspector'];
		if (parent) title.push(parent.name);

		title.push(doc.name);
		return title.join(': ');
	}

	get id() {
		return 'data-inspector-' + this.document.uuid.replace('.', '-');
	}

	get template() { return `modules/${CFG.id}/template/inspector.hbs`; }

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: [...options.classes, 'koboldworks', 'data-inspector'],
			resizable: true,
			width: 520,
			height: 620,
			sheetConfig: false,
			scrollY: ['div.data'],
		};
	}

	/*
	// Deny header buttons
	_getHeaderButtons() {
		return super._getHeaderButtons().filter(b => b.class === 'close');
	}
	*/

	getDataVariant(doc, mode) {
		const v11 = game.release.generation >= 11;

		switch (mode ?? this._mode) {
			case 'flags':
				if (this._flagData == null) this._flagData = doc.flags ?? {};
				return { data: this._flagData, path: 'flags' };
			case 'override':
				if (this._overrides == null) this._overrides = (v11 ? doc.token?.delta.toObject().system : doc.token?.actorData?.system) ?? {};
				return { data: this._overrides, path: 'system' };
			case 'rolldata':
				if (this._rollData == null) this._rollData = doc.getRollData();
				return { data: this._rollData, path: '' };
			case 'source':
				if (this._sourceData == null) this._sourceData = doc.toObject().system;
				return { data: this._sourceData, path: 'system' };
			default:
			case 'derived': {
				if (this._derivedData == null) this._derivedData = doc.system;
				return { data: this._derivedData, path: 'system' };
			}
		}
	}

	getBaseData() {
		const doc = this.document;
		switch (this._mode) {
			case 'flags':
				return doc.flags ?? {};
			case 'override':
				return doc.token?.actorData ?? {};
			case 'rolldata':
				return doc.getRollData();
			case 'source':
				return doc.toObject();
			default:
			case 'derived':
				return doc;
		}
	}

	getData() {
		const context = {},
			doc = this.document,
			isActor = doc instanceof Actor,
			type = doc.type,
			typeLabelKey = `${isActor ? 'ACTOR' : 'ITEM'}.Type${type.capitalize()}`,
			typeLabel = game.i18n.localize(typeLabelKey);

		if (this._mode === 'source') {
			this._temporaryData ??= createTemporaryDocument(this.document).toObject();
		}

		// Include prefix to mimic other data structures for easier handling all around (no need for special cases)
		context.rolldata = this.getDataVariant(doc, 'rolldata').data;
		context.sourcedata = this.getDataVariant(doc, 'source').data;
		context.derived = this.getDataVariant(doc, 'derived').data;
		if (isActor) context.overrides = this.getDataVariant(doc, 'override').data;
		context.flags = this.getDataVariant(doc, 'flags').data;

		const { data: docData, path: basepath } = this.getDataVariant(doc);

		context.document = doc;
		// data.overrides = doc.overrides;
		context.type = type;
		context.typeLabel = typeLabel;
		context.docType = doc.constructor.name;
		context.dataType = this._mode;
		context.isRollData = this._mode === 'rolldata';
		context.isSourceData = this._mode === 'source';
		context.isDerivedData = this._mode === 'derived';
		context.isOverrideData = this._mode === 'override';
		context.resultQuality = this._resultQuality;
		const isRollData = this._mode === 'rolldata';
		const isFlags = this._mode === 'flags';

		const basePath = isRollData ? null : 'system';
		const { root, all, count, depth } = DataStructure.recurse(docData, basepath, basepath, this._mode, { includeFunctions: this._functions, document: doc });
		context.data = root;
		this.root = root;
		context.count = count;
		context.depth = depth;

		context.model = root.isModel ? root.className : game.i18n.localize('DataInspector.NotAvailable');
		if (root.treetype === 'source')
			context.model = doc.system instanceof foundry.abstract.DataModel ? doc.system.constructor.name : game.i18n.localize('DataInspector.NotAvailable');

		context.dataTypes = {
			rolldata: 'DataInspector.Data.Roll',
			derived: 'DataInspector.Data.Derived',
			source: 'DataInspector.Data.Source',
			override: 'DataInspector.Data.Override',
			flags: 'DataInspector.Data.Flags',
		};
		if (!context.data.hasOverrides) delete context.dataTypes.override;

		root._sourceData = context.sourcedata;
		root._derivedData = context.derived;
		root._rollData = context.rolldata;
		root._overrides = context.overrides;
		root._temporaryData = this._temporaryData?.system;

		context.uuid = doc.uuid;
		context.search = this._search;
		context.searchEnd = this._searchEnd;
		context.includeFunctions = this._functions;
		context.includeDocument = this._document;
		context.path = this._path;

		context.TYPES = DATA_TYPES;

		return context;
	}

	/**
	 * @param {Element} el
	 * @param {ElementHandler} callback
	 * @param {...any} args
	 */
	treeWalker(el, callback, ...args) {
		callback(el, ...args);
		const parent = el.parentElement;
		if (parent && ['LI', 'UL'].includes(parent.tagName))
			this.treeWalker(parent, callback, ...args);
	}

	/**
	 * @param {Element} el
	 * @param {boolean} enabled
	 */
	highlightEl(el, enabled = true) {
		// console.log('Highlighting!');
		if (this._pathEl) this._pathEl.classList.toggle('selected', false);
		el.classList.toggle('selected', enabled);
		this._pathEl = el;
	}

	/**
	 * @param {Element} el
	 */
	expandDataElTree(el) {
		this.treeWalker(el, this.expandDataEl, false, true);
	}

	/**
	 * @param {Element} el
	 * @param {boolean} record
	 * @param {boolean} expand
	 */
	expandDataEl(el, record = true, expand = undefined) {
		el.classList.toggle('hide', false);

		if (el.classList.contains('value') || el.classList.contains('empty')) {
			// console.log('Don\'t expand empty or value entry');
			return;
		}

		// Record open state
		if (record) {
			const path = el.dataset.path;
			const items = this._expandedItems;
			el.classList.contains('collapsed') ? items.add(path) : items.delete(path);
		}

		// Toggle collapse
		el.classList.toggle('collapsed', expand !== undefined ? !expand : undefined);
		el.classList.toggle('expanded', expand);
	}

	/**
	 * @param {Element} el
	 * @param hide
	 * @param includeValues
	 */
	collapseAndClearEl(el, hide = false, includeValues = false) {
		el.classList.toggle('hide', hide);
		el.dataset.matchQuality = null;
		el.classList.toggle('selected', false);
		if (el.classList.contains('value') || el.classList.contains('empty')) return;
		el.classList.toggle('collapsed', true);
		el.classList.toggle('expanded', false);
	}

	/**
	 * @param {Event} ev
	 * @param {Element} el
	 */
	expandData(ev, el) {
		ev.preventDefault();
		ev.stopPropagation();
		this.expandDataEl(el);
	}

	/**
	 * @param {JQuery<HTMLElement>} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const html = jq[0];

		const header = html.querySelector('.header');

		const appEl = html.closest('[data-appid]');
		if (appEl) {
			const doc = this.document;
			const item = doc instanceof Item ? doc : null;
			if (item) {
				appEl.dataset.itemId = item.id;
				appEl.dataset.itemUuid = item.uuid;
			}

			const actor = doc instanceof Actor ? doc : this.document.parent;
			if (actor) {
				appEl.dataset.actorId = actor.id;
				appEl.dataset.actorUuid = actor.uuid;
			}
		}

		const pathInput = header.querySelector('input.path');
		const app = this;
		const rolldata = this._mode === 'rolldata';
		const rawdata = this._mode === 'source';

		/**
		 * @param {Event} ev
		 */
		function changeDataSource(ev) {
			const value = ev.target.value;
			if (app._mode !== value) {
				ev.preventDefault();
				ev.stopPropagation();
				ev.target.disabled = true;

				// Convert path to new mode
				const path = app._path;
				if (path.length > 0) {
					const pathp = path.split('');
					const prefix = 'system.';
					if (path[0] === '@' && app._mode === 'rolldata')
						pathp.splice(0, 1, prefix);
					else if (path.length > 5 && path.startsWith(prefix) && value === 'rolldata')
						pathp.splice(0, prefix.length, '@');
					app._path = pathp.join('');
				}

				// Swap mode
				app._path = app.getBasePath(app._path, app._mode);
				app._mode = value;
				app._lastSearch = undefined;
				app.render(false, { keepCache: true });
			}
		}

		header.querySelector('select.type').addEventListener('change', changeDataSource);

		function changeSearchQuality(ev) {
			// console.log('Quality:', this.value);
			app._resultQuality = Number(this.value);
			if (app._search.length > 0) app.doSearch(app._search, { forceRefresh: true });
		}

		header.querySelector('input.search-quality').addEventListener('change', changeSearchQuality);

		/**
		 * @param {Event} ev
		 */
		function copyPath(ev) {
			ev.preventDefault();
			ev.stopPropagation();
			const path = this.dataset.path ?? this.closest('[data-path]').dataset.path;
			const properPath = rolldata ? `@${path}` : path;
			pathInput.value = properPath;
			if (!properPath)
				return void console.error('Failed to parse path from', { path, element: this });
			app._path = properPath;
			app.highlightEl(this, true);
			console.log(`Saving path %c${path}%c to clipboard`, CFG.COLORS.id, CFG.COLORS.unset);
			game.clipboard.copyPlainText(properPath)
				.then(() => ui.notifications.info(game.i18n.format('DataInspector.Info.CopyPath', { path })));
		}

		/**
		 * @param {Event} ev
		 */
		function copyValue(ev) {
			ev.preventDefault();
			ev.stopPropagation();
			const path = this.closest('[data-path]').dataset.path;
			app._currentData ??= app.getBaseData();
			let value = fu.getProperty(app._currentData, path);
			if (!['string', 'number'].includes(typeof value)) value = JSON.stringify(value, null, 2);
			console.log(`Saving value of %c${path}%c to clipboard:`, CFG.COLORS.id, CFG.COLORS.unset, { value });
			game.clipboard.copyPlainText(value)
				.then(() => ui.notifications.info(game.i18n.format('DataInspector.Info.CopyValue', { path })));
		}

		/**
		 * @param {Event} ev
		 */
		function expandPath(ev) {
			ev.preventDefault();
			ev.stopPropagation();

			app.expandData(ev, this);
		}

		jq.on('click', '[data-path]', expandPath);
		jq.on('contextmenu', '[data-path] .details .key', copyPath);
		jq.on('contextmenu', '[data-path] .details .value,[data-path] .details .extended-value', copyValue);
		pathInput.addEventListener('input', ev => this.debounceCustomPath(ev.target.value, ev, true));
		pathInput.addEventListener('change', ev => this.debounceCustomPath(ev.target.value, ev, false));

		// Open previously open paths
		html.querySelectorAll('[data-path]').forEach(el => {
			const path = el.dataset.path;
			if (this._expandedItems.has(path))
				this.expandDataEl(el);
		});

		/*
		html.querySelectorAll('.value[data-id]').forEach(el => {
			el.addEventListener('click', this._copyId);
		});
		*/

		const dataSection = html.querySelector('form > .data');
		dataSection.addEventListener('dblclick', function openEditor(ev) {
			ev.preventDefault();
			ev.stopPropagation();

			if (DataInspectorDialog.EDITABLE_MODES.includes(app._mode)) {
				const el = ev.target;
				const ael = el.dataset.path ? el : el.closest('[data-path]');
				if (ael) app._openValueEditor(ev, ael.dataset.path);
			}
			else {
				ui.notifications.warn(`Editing not supported in "${app._mode}" mode!`);
			}
		});
		/*
		dataSection.querySelectorAll('.data-element[data-path]').forEach(el => {
			const sel = el.querySelector('.details > .value');
			sel.addEventListener('dblclick', function (ev) { app._openValueEditor(ev, el.dataset.path); });
		});
		*/

		/**
		 * @param {Element} el
		 */
		const buildSearchCache = (el) => {
			const path = el.dataset.path;
			const key = el.dataset.key;
			const label = el.querySelector('h4');
			this.paths.set(path, {
				path,
				key,
				element: el,
				label: label.textContent,
			});
		};

		const useTooltips = game.settings.get(CFG.id, CFG.SETTINGS.tooltip);

		const paths = dataSection.querySelectorAll('li[data-path]');
		const pathPrefix = this._mode === 'rolldata' ? '@' : '';
		paths.forEach(el => {
			if (!useTooltips) return;
			const dEl = el.querySelector('.details');
			dEl?.addEventListener('mouseenter', async ev => {
				const path = el.dataset.path;

				let dpath = path;
				if (app._mode !== 'rolldata') {
					const parts = path.split('.');
					parts.shift();
					dpath = parts.join('.');
				}

				const dd = this.root?.getAtPath(dpath);
				const typeId = dd.typeId;
				const typeName = typeId !== DATA_TYPES.custom ? game.i18n.localize(`DataInspector.Types.${DATA_TYPE_INVERTED[typeId]}`) : dd.className;

				const isFlags = dd.treetype === 'flags';

				const templateData = {
					dd,
					path: pathPrefix + path,
					type: typeName,
					documentId: dd.documentId,
					isContainer: (dd.isContainer || dd.isDocument),
					children: (dd.isContainer || dd.isDocument) ? dd.children.length : null,
					isString: dd.isString,
					length: dd.isString ? dd.value.length : null,
					treetype: dd.treetype,
					notFlags: !isFlags,
					inData: {
						rolldata: {
							show: !isFlags && dd.treetype !== 'rolldata',
							label: 'DataInspector.Data.Roll',
							present: !isFlags ? dd.inRollData : false,
						},
						derived: {
							show: !isFlags && dd.treetype !== 'derived',
							label: 'DataInspector.Data.Derived',
							present: !isFlags ? dd.inDerivedData : false,
						},
						source: {
							show: !isFlags && dd.treetype !== 'source',
							label: 'DataInspector.Data.Source',
							present: !isFlags ? dd.inSourceData : false,
						},
						temporary: {
							show: dd.treetype === 'source',
							label: 'DataInspector.Data.Temporary',
							present: !isFlags ? dd.inTemporaryData : false,
						},
					},
				};

				const text = await renderTemplate(`modules/${CFG.id}/template/tooltip.hbs`, templateData);

				game.tooltip.activate(dEl, { text, direction: 'UP', cssClass: 'data-inspector-tooltip' });
			}, { passive: true });

			dEl?.addEventListener('mouseleave', () => game.tooltip.deactivate(), { passive: true });
		});

		const search = header.querySelector('input.search');
		search.addEventListener('change', ev => this.debounceSearch(ev.target.value, ev, false), { passive: true });
		search.addEventListener('input', ev => this.debounceSearch(ev.target.value, ev, true), { passive: true });

		this.paths = new Collection();
		paths.forEach(el => buildSearchCache(el));

		/*
		const sEnd = header.querySelector('input.search-end');
		sEnd.addEventListener('change', ev => {
			this._searchEnd = ev.target.checked;
			this.debounceSearch(this._search);
		});
		*/

		const functions = header.querySelector('input.functions');
		functions.addEventListener('change', ev => {
			ev.preventDefault();
			ev.stopPropagation();

			this._functions = ev.target.checked;
			this.render();
		});

		/*
		const indocument = header.querySelector('input.document');
		indocument.addEventListener('change', ev => {
			this._document = ev.target.checked;
			this.render();
		});
		*/

		if (this._search.length) this.debounceSearch(this._search);

		if (this._functions) {
			if (!this._getGetterValueBound) this._getGetterValueBound = this._getGetterValue.bind(this);
			dataSection.addEventListener('click', function (ev) {
				if (!ev.target.matches('.getter.value')) return;
				ev.preventDefault();
				ev.stopPropagation();
				app._getGetterValueBound(ev);
			});
		}

		// Always expand selected path
		this.expandSelectedPath();
	}

	/**
	 * @param {Event} event
	 * @param {string} path
	 */
	_openValueEditor(event, path) {
		if (!game.settings.get(CFG.id, CFG.SETTINGS.allowEditing))
			return ui.notifications.warn('Editing has been disabled.');

		const pp = path.split('.');
		const doc = this.document;
		let currentData = doc;

		let type;
		let inArray = false;
		while (pp.length > 0) {
			const part = pp.shift();
			type = getChildTypeId(currentData, part);
			if (type === DATA_TYPES.getter) return ui.notifications.error(game.i18n.localize('DataInspector.Error.NoGetterEdit'));

			currentData = currentData[part];
			if (currentData == null && pp.length > 0)
				return ui.notifications.warn(game.i18n.format('DataInspector.Error.NavigationFailure', { path }));
			if (Array.isArray(currentData) && pp.length > 0) {
				inArray = true;
				// return ui.notifications.warn(`Editing arrays or values inside of arrays not supported: ${path}`);
			}
		}

		const isSupported = ValueEditor.supportedTypes.includes(type);
		if (!isSupported) {
			const allowUnsupported = game.settings.get(CFG.id, CFG.SETTINGS.allowEditingUnsupported);
			if (!allowUnsupported)
				return ui.notifications.warn(game.i18n.format('DataInspector.Error.UnsupportedEditType', { type: DATA_TYPE_INVERTED[type] }));
		}

		new ValueEditor(this.document, path, type, { isToken: this._mode === 'override', app: this, inArray })
			.render(true, { focus: true });
	}

	/**
	 * @param {Event} _event
	 */
	_copyId(_event) {
		const id = this.dataset.id;
		console.log('Copying ID:', id);
		return game.clipboard.copyPlainText(id);
	}

	_getGetterValueBound;

	/**
	 * @param {Event} event
	 */
	_getGetterValue(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		const target = event.target;
		if (!target) return;
		target.removeEventListener('click', this._getGetterValueBound);
		this._resolveGetter(target);
	}

	/**
	 * @param {Element} element
	 */
	_resolveGetter(element) {
		const el = element.dataset.path ? element : element.closest('[data-path]');
		if (!el) return;
		/** @type {string} */
		const path = el.dataset.path;

		const ds = this.root.getAtPath(path);
		const value = ds.value;
		ds.typeId = getTypeId(value); // Make sure tooltips detect correct type

		element.textContent = value;
		element.classList.add('resolved');
		element.classList.add(ds.type);
	}

	/**
	 * Strip @ and data. prefix
	 *
	 * @param {string} path
	 * @param {"rolldata"|"source"|"derived"|"override"} oldMode
	 * @param {"rolldata"|"source"|"derived"|"override"} targetMode
	 * @returns {string}
	 */
	getBasePath(path, oldMode, targetMode) {
		oldMode ??= this._mode;
		if (oldMode === 'rolldata')
			return path.replace(/^@/, '');
		if (targetMode === 'rolldata')
			return path.replace(/^system\./, '@');
		return path;
	}

	expandSelectedPath() {
		if (this._path.length == 0) return;

		const path = this.getBasePath(this._path);

		// console.log({ path, source: this._path, mode: this._mode });
		const p = this.paths.get(path);
		if (p) {
			this.expandDataElTree(p.element);
			this._pathEl = p.element;
			this.highlightEl(this._pathEl, true);
			this._pathEl.scrollIntoView({ block: 'center' });
		}
	}

	inputDebounceDelay = 250;
	searchDelayTimer;
	selectDelayTimer;

	/**
	 * @param {string} search
	 * @param {Event} event
	 * @param {boolean} active
	 */
	debounceSearch(search, event, active = false) {
		clearTimeout(this.searchDelayTimer);
		// Increase delay for short queries and shorten it for end of input
		if (search.length === 0) active = false;
		const delay = Math.clamped(active ? search.length > 3 ? this.inputDebounceDelay : this.inputDebounceDelay * 2 : this.inputDebounceDelay / 2, 150, 500);
		this.searchDelayTimer = setTimeout(_ => this.doSearch(search), delay);
	}

	/**
	 * @param {string} input
	 * @param {Event} event
	 * @param {boolean} active
	 */
	debounceCustomPath(input, event, active = false) {
		this._path = input;
		if (this._pathEl) {
			this._pathEl.classList.toggle('selected', false);
			this._pathEl = null;
		}
		clearTimeout(this.selectDelayTimer);
		if (input.length === 0) active = false;
		const delay = active ? input.length > 3 ? this.inputDebounceDelay : this.inputDebounceDelay * 2 : this.inputDebounceDelay / 2;
		// console.log('SelectDebounce:', search, delay);
		this.selectDelayTimer = setTimeout(_ => this.selectCustomPath(input), delay);
	}

	/**
	 * @param {string} input
	 */
	selectCustomPath(input) {
		this._path = input;
		this.expandSelectedPath();
		const path = this.paths.get(input);
		// console.log('SELECTING:', input, path);
		if (path) {
			this.expandDataElTree(path.element);
			this.highlightEl(path.element);
			this._pathEl = path.element;
		}
		else {
			this._pathEl = null;
		}
	}

	_lastSearch;
	lastEndSerch;
	/**
	 * @param {string} search
	 * @param {object} options
	 * @param {boolean} options.forceRefresh
	 */
	doSearch(search, { forceRefresh = false } = {}) {
		this._search = search;
		console.log('Searching:', this._search);

		if (forceRefresh !== true && this._lastSearch === search && this.lastEndSerch === this._searchEnd) return;

		this._lastSearch = search;
		this.lastEndSerch = this._searchEnd;

		if (search.length == 0) {
			this.paths.forEach(p => this.collapseAndClearEl(p.element, false));
			this.expandSelectedPath();
			return;
		}

		// console.log('DATA INSPECTOR |', Date.now(), 'Searching:', search, 'EndFocus:', this._searchEnd);

		// Collapse all first to ensure expansion happens smoothly
		this.paths.forEach(p => this.collapseAndClearEl(p.element, true));

		const term = search.toLowerCase().split('');

		/**
		 * Do actual search
		 *
		 * @param {string} term
		 * @param {string} subject
		 * @param {object} options
		 * @param {boolean} options.harsh
		 */
		function matchTerm(term, subject, { harsh = true } = {}) {
			const indexes = [];
			const subjectParts = subject.toLowerCase().split('');
			let consecutives = 0, longestChain = 0;
			let si = 0, sm = term[0], lastMatch = -2;
			let lastChain = 1;
			const scoring = [];
			const updateLongestChain = () => {
				if (lastChain > longestChain) longestChain = lastChain;
				lastChain = 1;
			};
			for (let i = 0; i < subjectParts.length; i++) {
				if (subjectParts[i] === sm) {
					indexes.push(i);
					// Give score for subsequent matches
					if (lastMatch === i - 1) {
						scoring.push(new Score(3, `Consecutive [${i}]: +3`));
						consecutives++;
						lastChain++;
					}
					else {
						scoring.push(new Score(1, `Match [${i}]: +1`));
						updateLongestChain();
					}
					lastMatch = i;
					// Add score if match is closer to the end
					const endBonus = Math.clamped(Math.round((subjectParts.length - i) / 5), 0, 3);
					if (endBonus > 0) scoring.push(new Score(endBonus, `End bonus: +${endBonus}`));
					// End when there's nothing left to search
					sm = term[++si];
					if (sm === undefined) break;
				}
			}
			updateLongestChain();

			// Basic score for matched letters
			if (indexes.length > 0) scoring.push(new Score(indexes.length * 3, `Matched letters bonus: ${indexes.length * 3}`));
			else scoring.push(new Score(-100, 'No matches: -100'));

			// Offset less accurate matches
			/*
			score -= subjectParts.length;
			scoring.push(`Accuracy penalty: -${subjectParts.length}`);
			*/

			// Search term found fully
			const full = indexes.length == term.length;
			if (full) scoring.push(new Score(20, 'Full match: +20'));

			// Reduce score of search terms that are matched only partially
			const half = indexes.length < Math.floor(term.length / 2) - 2;
			if (harsh) {
				const cumulative = scoring.reduce((prev, cur) => cur.value + prev),
					halfScore = -Math.floor(cumulative / 2);
				if (half) scoring.push(new Score(halfScore, `Partial match: ${halfScore}`));
				if (longestChain <= 1) scoring.push(new Score(-20, 'Short chains: -20'));
			}

			return new TermMatch({ indexes, full, half, ratio: (consecutives + 1) / term.length, consecutives, longestChain, scoring });
		}

		let fullMatches0 = 0, fullMatches1 = 0;

		// Extremely simple search
		// TODO: Allow skipping letters in search term but impose negative score if that is the only way to get them to match.
		const results = this.paths.reduce((results, target) => {
			const matchPath = matchTerm(term, target.path),
				matchKey = matchTerm(term, target.key);

			if (matchKey.full) fullMatches0++;
			if (matchPath.full) fullMatches1++;

			let match = matchPath.ratio > 0.5,
				ratio = matchPath.ratio;

			if (this._keyFocus) {
				matchPath.score /= 5;
				ratio = matchKey.ratio;
				match = matchKey.ratio > matchPath.ratio;
			}
			else {
				ratio += matchKey.ratio * 2;
				ratio /= 3;
			}

			results.push(new SearchResult({
				path: target.path,
				match,
				target,
				keyFocus: this._keyFocus,
				score: matchPath.score / 2 + matchKey.score * 2,
				keyFullMatch: matchKey.full,
				pathFullMatch: matchPath.full,
				pathIndexes: matchPath.indexes,
				keyIndexes: matchKey.indexes,
				ratio,
				scoring: { full: matchPath.scoring, key: matchKey.scoring },
			}));

			return results;
		}, []);

		results.sort((a, b) => {
			if (a.full && !b.full) return -1;
			if (b.full && !a.full) return 1;
			return b.score - a.score;
		});

		const Break = {};

		// Format results
		try {
			results.forEach((r) => {
				const el = r.target.element;
				el.dataset.searchScore = `${r.score}`;
				el.dataset.searchRatio = `${r.ratio}`;

				let q = 'decent';
				if (r.mismatch) q = 'mismatch';
				else if (r.bad) q = 'bad';
				else if (r.good) q = 'good';
				el.dataset.matchQuality = q;

				if (fullMatches0 > 0) {
					if (r.ratio > this._resultQuality) this.expandDataElTree(el);
				}
				else if (!r.bad)
					this.expandDataElTree(el);

				// if (!r.mismatch) console.log(r.path, { score: r.score }, { r, quality: el.dataset.matchQuality });

				// if (index > 20) throw Break;
			});
		}
		catch (err) {
			if (err !== Break) throw err;
		}

		this.expandSelectedPath();
	}

	render(force = false, options = {}) {
		if (options.action === 'update') options.keepCache = false;
		if (options.keepCache !== true) {
			this._currentData = undefined;
			this._rollData = undefined;
			this._sourceData = undefined;
			this._derivedData = undefined;
			this._overrides = undefined;
			this._flagData = undefined;
			this._lastSearch = undefined;
		}

		return super.render(force, options);
	}
}
