import { CFG } from './config.mjs';
import { DataInspectorDialog } from './inspection-dialog.mjs';

const refreshActiveDialogs = () => Object.values(ui.windows)
	.forEach(app => app instanceof DataInspectorDialog ? app.render() : null);

Hooks.once('init', function registerSettings() {
	game.settings.register(CFG.id, CFG.SETTINGS.mode, {
		name: 'DataInspector.Settings.DefaultMode',
		hint: 'DataInspector.Settings.DefaultModeHint',
		scope: 'client',
		config: true,
		type: String,
		default: 'rolldata',
		choices: {
			rolldata: 'DataInspector.Data.Roll',
			source: 'DataInspector.Data.Source',
			derived: 'DataInspector.Data.Derived',
		},
	});

	game.settings.register(CFG.id, CFG.SETTINGS.results, {
		name: 'DataInspector.Settings.ResultQuality',
		hint: 'DataInspector.Settings.ResultQualityHint',
		scope: 'client',
		config: true,
		type: Number,
		default: 0.7,
		range: { min: 0.0, max: 1.0, step: 0.01 },
	});

	game.settings.register(CFG.id, CFG.SETTINGS.functions, {
		name: 'DataInspector.Settings.IncludeFunctions',
		hint: 'DataInspector.Settings.IncludeFunctionsHint',
		scope: 'client',
		config: true,
		type: Boolean,
		default: false,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.allowEditing, {
		name: 'DataInspector.Settings.AllowEditing',
		hint: 'DataInspector.Settings.AllowEditingHint',
		config: true,
		scope: 'world',
		type: Boolean,
		default: false,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.allowEditingUnsupported, {
		name: 'DataInspector.Settings.AllowEditingUnsupported',
		hint: 'DataInspector.Settings.AllowEditingUnsupportedHint',
		config: true,
		scope: 'world',
		type: Boolean,
		default: false,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.allowUsers, {
		name: 'DataInspector.Settings.AllowUsers',
		hint: 'DataInspector.Settings.AllowUsersHint',
		config: true,
		scope: 'world',
		type: Boolean,
		default: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.tooltip, {
		name: 'DataInspector.Settings.Tooltip',
		hint: 'DataInspector.Settings.TooltipHint',
		config: true,
		scope: 'client',
		type: Boolean,
		default: true,
		onChange: () => refreshActiveDialogs(),
	});
});
